<?php

class CheckoutNotification extends OMagicMethodAttributes
{
    /** @var Cart */
    protected $cart;
    public static $classHandler = __CLASS__;

    public function __construct(Cart $cart)
    {
        $this->cart = $cart;
        $this->setAttributes($cart->getAttributes());
    }

    public static function createInstance(Cart $cart)
    {
        return new self::$classHandler($cart);
    }

    public function sendSms($wasNew = false)
    {
        if ($this->cart->send_notification) {
            $oldCurrency = currency_id();
            currency_init($this->cart->getCurrency()->id);
            $totalSum = $this->cart->total_sum + (($this->cart->delivery_price >= 0) ? $this->cart->delivery_price : 0) + $this->cart->payment_price;

            if ($this->getAttribute('stat_manager') != 0) {
                $sendNotification = $this->cart->getAttribute('send_notification');
                if ($wasNew && !empty($sendNotification)) {
                    SmsTemplates::sendSms(
                        $this->delivery_phone,
                        SmsTemplates::TYPE_CHECKOUT,
                        [
                            '{ORDER_ID}'    => $this->getAttribute('order_id'),
                            '{USERNAME}'    => $this->delivery_name,
                            '{LOGIN}'       => $this->delivery_email,
                            '{DOMAIN}'      => idn_to_utf8(SERVER_ADDR),
                            '{LINK}'        => PROTOCOL_PREFIX . idn_to_utf8(SERVER_ADDR),
                            '{priceTotal}'  => currency_wrap_html(priceFormat($totalSum)),
                            '{TOTAL_PRICE}' => currency_wrap_html(priceFormat($totalSum))
                        ]
                    );
                }
            } else {
                SmsTemplates::sendSms(
                    $this->delivery_phone,
                    SmsTemplates::TYPE_CHECKOUT,
                    [
                        '{ORDER_ID}'    => $this->getAttribute('order_id'),
                        '{USERNAME}'    => $this->delivery_name,
                        '{LOGIN}'       => $this->delivery_email,
                        '{DOMAIN}'      => idn_to_utf8(SERVER_ADDR),
                        '{LINK}'        => PROTOCOL_PREFIX . idn_to_utf8(SERVER_ADDR),
                        '{priceTotal}'  => currency_wrap_html(priceFormat($totalSum)),
                        '{TOTAL_PRICE}' => currency_wrap_html(priceFormat($totalSum))
                    ]
                );
            }

            currency_init($oldCurrency);
        }
    }

    public function sendEmail($wasNew = false)
    {
        $sendEmail = false;
        $sendNotification = $this->cart->getAttribute('send_notification');
        if ($wasNew && $this->cart->getAttribute('stat_manager') != 0 && !empty($sendNotification)) {
            $sendEmail = true;
        }
        $email = $this->delivery_email;
        $user = User::getInstance($this->cart->user);
        if ($this->cart->send_notification && (($sendEmail && (checkMail($email) || $user->virtual)) || checkMail($email) || $user->virtual)) {
            // Формирование сообщения о доставке
            /*  */
            $userInfo = array_merge($this->getUserInfo(), $this->getDeliveryInfo());


            $fields = [
                ['type' => MailWidget::TYPE_HEADING, 'content' => l10n("Заказ №{ORDER_NUMBER}", ['{ORDER_NUMBER}' => $this->getAttribute('order_id')])],

            ];

            $payment = PaymentType::findById($this->payment_type);
            if ($payment && trim(strip_tags($payment->payment_instruction))) {
                $fields[] = ['type' => MailWidget::TYPE_HTML, 'content' => $payment->payment_instruction];
            }

            $fields[] = ['type' => MailWidget::TYPE_TABLE, 'content' => $userInfo];
            $fields[] = ['type' => MailWidget::TYPE_ORDER_TABLE, 'title' => l10n("Содержание заказа"), 'content' => $this->cart];

            $mail = new MailWidget([
                'subject' => l10n("Вы оформили заказ №{ORDER_NUMBER} на сайте {DOMAIN}", ['{ORDER_NUMBER}' => $this->cart->order_id]),
                'data'    => $fields
            ]);
//            TODO
//            echo $mail->render();
//            die();
            if (!$user->virtual) {
                $mail->send($this->delivery_email, null, Z::$app->settings->email_reply_to);
            }


            $mail = new MailWidget([
                'subject' => l10n("Оформлен новый заказ на сайте {DOMAIN} №{ORDER_NUMBER}", ['{DOMAIN}' => idn_to_utf8(SERVER_ADDR), '{ORDER_NUMBER}' => $this->getAttribute('order_id')]),
                'data'    => [
                    ['type' => MailWidget::TYPE_HEADING, 'content' => l10n("Заказ №{ORDER_NUMBER}", ['{ORDER_NUMBER}' => $this->getAttribute('order_id')])],
                    ['type' => MailWidget::TYPE_TABLE, 'content' => $userInfo],
                    ['type' => MailWidget::TYPE_ORDER_TABLE, 'title' => l10n("Содержание заказа"), 'content' => $this->cart]
                ]
            ]);
            $this->notifyAdmins($mail, $email);


        }
    }

    public function notifyAdmins($mail, $reply = null)
    {
        _admins_send_mail($mail, [], $reply);
    }

    public function getUserInfo()
    {
        $rows = [
            l10n("Имя и фамилия") => $this->delivery_name,
            l10n("Email")         => $this->delivery_email,
            l10n("Телефон")       => $this->delivery_phone,
        ];

        return $rows;
    }

    public function getDeliveryInfo()
    {
        //$rows = array(array("Тип доставки", viewBookValue("h_delivery", intval($this->delivery_type))));
        $rows = [];
        if (trim($this->delivery_country) !== "") {
            $rows[l10n("Страна")] = $this->delivery_country;
        }
        if (trim($this->delivery_city) !== "") {
            $rows[l10n("Город")] = $this->delivery_city;
        }
        if ($this->delivery_type != 0) {
            $rows[l10n("Способ доставки")] = viewBookValue("h_delivery", intval($this->delivery_type), 'title', l10n_id());
        }
        if (trim($this->delivery_address) !== "") {
            $rows[l10n("Адрес")] = implode(', ', explode('@', $this->delivery_address));
        }
        if ($this->payment_type != 0) {
            $payment = PaymentType::findById($this->payment_type);
            $rows[l10n("Способ оплаты")] = $payment->title;
        }
        if (trim($this->comment) !== "") {
            $rows[l10n("Комментарий")] = $this->comment;
        }
        if (Z::$app->settings->getAttribute('order_without_callback')) {
            $rows[l10n("Перезвонить")] = $this->order_without_callback ? l10n('Нет') : l10n('Да');
        }

        return $rows;
    }


    public function hookServices()
    {

        $export = new \horoshop\order\Export(isset(Z::$app->hs->checkout->use_countries) && Z::$app->hs->checkout->use_countries, false);

        $orders = $export->id($this->cart->order_id)->process();

        if ($orders) {
            $order = array_shift($orders);
            \horoshop\hooks\Service::getInstance()->hook('order_created', $order);
        }
    }

    public function getId()
    {
        return $this->cart->id;
    }
}
