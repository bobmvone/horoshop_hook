<?php

define ("B24_DOMAIN",'compbest.bitrix24.ua');
class CheckoutNotification extends OMagicMethodAttributes
{
    /** @var Cart */
    protected $cart;
    public static $classHandler = __CLASS__;

    public function __construct(Cart $cart)
    {
        $this->cart = $cart;
        $this->setAttributes($cart->getAttributes());
    }

    public static function createInstance(Cart $cart)
    {
        return new self::$classHandler($cart);
    }

    public function sendSms($wasNew = false)
    {
        if ($this->cart->send_notification) {
            $oldCurrency = currency_id();
            currency_init($this->cart->getCurrency()->id);
            $totalSum = $this->cart->total_sum + (($this->cart->delivery_price >= 0) ? $this->cart->delivery_price : 0) + $this->cart->payment_price;

            if ($this->getAttribute('stat_manager') != 0) {
                $sendNotification = $this->cart->getAttribute('send_notification');
                if ($wasNew && !empty($sendNotification)) {
                    SmsTemplates::sendSms(
                        $this->delivery_phone,
                        SmsTemplates::TYPE_CHECKOUT,
                        [
                            '{ORDER_ID}'    => $this->getAttribute('order_id'),
                            '{USERNAME}'    => $this->delivery_name,
                            '{LOGIN}'       => $this->delivery_email,
                            '{DOMAIN}'      => idn_to_utf8(SERVER_ADDR),
                            '{LINK}'        => PROTOCOL_PREFIX . idn_to_utf8(SERVER_ADDR),
                            '{priceTotal}'  => currency_wrap_html(priceFormat($totalSum)),
                            '{TOTAL_PRICE}' => currency_wrap_html(priceFormat($totalSum))
                        ]
                    );
                }
            } else {
                SmsTemplates::sendSms(
                    $this->delivery_phone,
                    SmsTemplates::TYPE_CHECKOUT,
                    [
                        '{ORDER_ID}'    => $this->getAttribute('order_id'),
                        '{USERNAME}'    => $this->delivery_name,
                        '{LOGIN}'       => $this->delivery_email,
                        '{DOMAIN}'      => idn_to_utf8(SERVER_ADDR),
                        '{LINK}'        => PROTOCOL_PREFIX . idn_to_utf8(SERVER_ADDR),
                        '{priceTotal}'  => currency_wrap_html(priceFormat($totalSum)),
                        '{TOTAL_PRICE}' => currency_wrap_html(priceFormat($totalSum))
                    ]
                );
            }

            currency_init($oldCurrency);
        }
    }

    public function sendEmail($wasNew = false)
    {
        $sendEmail = false;
        $sendNotification = $this->cart->getAttribute('send_notification');
        if ($wasNew && $this->cart->getAttribute('stat_manager') != 0 && !empty($sendNotification)) {
            $sendEmail = true;
        }
        $email = $this->delivery_email;
        $user = User::getInstance($this->cart->user);
        if ($this->cart->send_notification && (($sendEmail && (checkMail($email) || $user->virtual)) || checkMail($email) || $user->virtual)) {
            // Формирование сообщения о доставке
            /*  */
            $userInfo = array_merge($this->getUserInfo(), $this->getDeliveryInfo());


            $fields = [
                ['type' => MailWidget::TYPE_HEADING, 'content' => l10n("Заказ №{ORDER_NUMBER}", ['{ORDER_NUMBER}' => $this->getAttribute('order_id')])],

            ];

            $payment = PaymentType::findById($this->payment_type);
            if ($payment && trim(strip_tags($payment->payment_instruction))) {
                $fields[] = ['type' => MailWidget::TYPE_HTML, 'content' => $payment->payment_instruction];
            }

            $fields[] = ['type' => MailWidget::TYPE_TABLE, 'content' => $userInfo];
            $fields[] = ['type' => MailWidget::TYPE_ORDER_TABLE, 'title' => l10n("Содержание заказа"), 'content' => $this->cart];

            $mail = new MailWidget([
                'subject' => l10n("Вы оформили заказ №{ORDER_NUMBER} на сайте {DOMAIN}", ['{ORDER_NUMBER}' => $this->cart->order_id]),
                'data'    => $fields
            ]);
//            TODO
//            echo $mail->render();
//            die();
            if (!$user->virtual) {
                $mail->send($this->delivery_email, null, Z::$app->settings->email_reply_to);
            }


            $mail = new MailWidget([
                'subject' => l10n("Оформлен новый заказ на сайте {DOMAIN} №{ORDER_NUMBER}", ['{DOMAIN}' => idn_to_utf8(SERVER_ADDR), '{ORDER_NUMBER}' => $this->getAttribute('order_id')]),
                'data'    => [
                    ['type' => MailWidget::TYPE_HEADING, 'content' => l10n("Заказ №{ORDER_NUMBER}", ['{ORDER_NUMBER}' => $this->getAttribute('order_id')])],
                    ['type' => MailWidget::TYPE_TABLE, 'content' => $userInfo],
                    ['type' => MailWidget::TYPE_ORDER_TABLE, 'title' => l10n("Содержание заказа"), 'content' => $this->cart]
                ]
            ]);
            $this->notifyAdmins($mail, $email);


        }
    }

    public function notifyAdmins($mail, $reply = null)
    {
        _admins_send_mail($mail, [], $reply);
    }

    public function getUserInfo()
    {
        $rows = [
            l10n("Имя и фамилия") => $this->delivery_name,
            l10n("Email")         => $this->delivery_email,
            l10n("Телефон")       => $this->delivery_phone,
        ];

        return $rows;
    }

    public function getDeliveryInfo()
    {
        //$rows = array(array("Тип доставки", viewBookValue("h_delivery", intval($this->delivery_type))));
        $rows = [];
        if (trim($this->delivery_country) !== "") {
            $rows[l10n("Страна")] = $this->delivery_country;
        }
        if (trim($this->delivery_city) !== "") {
            $rows[l10n("Город")] = $this->delivery_city;
        }
        if ($this->delivery_type != 0) {
            $rows[l10n("Способ доставки")] = viewBookValue("h_delivery", intval($this->delivery_type), 'title', l10n_id());
        }
        if (trim($this->delivery_address) !== "") {
            $rows[l10n("Адрес")] = implode(', ', explode('@', $this->delivery_address));
        }
        if ($this->payment_type != 0) {
            $payment = PaymentType::findById($this->payment_type);
            $rows[l10n("Способ оплаты")] = $payment->title;
        }
        if (trim($this->comment) !== "") {
            $rows[l10n("Комментарий")] = $this->comment;
        }
        if (Z::$app->settings->getAttribute('order_without_callback')) {
            $rows[l10n("Перезвонить")] = $this->order_without_callback ? l10n('Нет') : l10n('Да');
        }

        return $rows;
    }
    public function execCurlBitrixByWebhook($method, $var, $domain){
        //$queryUrl = 'https://' . $domain . '/rest/1/3idvdnhk7a4l8ag9/' . $method . '.json';
        $queryUrl = 'https://' . $domain . '/rest/2211/4msps7i3fixt4mj6/' . $method . '.json';
        $queryData = http_build_query($var);
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_POST => 1,
            CURLOPT_HEADER => 0,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $queryUrl,
            CURLOPT_POSTFIELDS => $queryData,
        ));

        $result = curl_exec($curl);
        $result = json_decode($result, true);
        curl_close($curl);

        return $result;
    }
    //***************************************
    public function findExistContactByPhone($phone){
        $var = array('filter'=>array('PHONE'=>$phone));
        $curl_result = $this->execCurlBitrixByWebhook('crm.contact.list',$var,B24_DOMAIN);
        if(count($curl_result['result'])!=0)
            return $curl_result['result'][0]['ID'];
        else
            return false;
    }
    public function getContact($id){
        $var = array('id'=>$id);
        $curl_result = $this->execCurlBitrixByWebhook('crm.contact.get',$var,B24_DOMAIN);
    }
    public function getDeal($id){
        $var = array('id'=>$id);
        return $curl_result = $this->execCurlBitrixByWebhook('crm.deal.get',$var,B24_DOMAIN);
    }
    public function createDeal($data)
    {
        $var = array('fields'=>$data,
            'params'=>array('REGISTER_SONET_EVENT'=>'Y'));
        return $curl_result = $this->execCurlBitrixByWebhook('crm.deal.add',$var,B24_DOMAIN);
    }
    public function createContact($data){
        $var = array('fields'=>array(
            'NAME'=>$data['NAME'],
            'OPENED'=>'Y',
            'ASSIGNED_BY_ID'=>$data['ASSIGNED'],
            'PHONE'=>array(array('VALUE'=>$data['PHONE'],'VALUE_TYPE'=>"WORK")),
            'EMAIL'=>array(array('VALUE'=>$data['EMAIL'],'VALUE_TYPE'=>"WORK")),
            'COMMENTS'=>$data['COMMENT']
        ),
            'params'=>array('REGISTER_SONET_EVENT'=>'Y'));
        $curl_result = $this->execCurlBitrixByWebhook('crm.contact.add',$var,B24_DOMAIN);
        return $curl_result;
    }
    public function main_b24_integration($order){
        $assigned_by_id = '2211';
        $json_order = json_encode($order,JSON_UNESCAPED_UNICODE);
        $phone = "+".preg_replace('~[^0-9]+~','',$order['delivery_phone']);
        $order_to_create_contact = array('NAME'=>$order['delivery_name'],'EMAIL'=>$order['delivery_email'],'PHONE'=>$phone,'COMMENT'=>'Город:'.$order['delivery_city'],'ASSIGNED'=>$assigned_by_id);
        $curl_result = $this->findExistContactByPhone($phone);
        $id_contact = 0;
        if($curl_result!=false)
        {
            $id_contact = $curl_result;
        }
        else
        {
            $create_contact_result = $this->createContact($order_to_create_contact);
            $id_contact = $create_contact_result['result'];
        }
        $comment = '';
        $comment .= '<br>Город: '.$order['delivery_city'];
        $comment .= '<br>Тип доставки: '.$order['delivery_type']['title'];
        $comment .= '<br>Адрес доставки: '.$order['delivery_address'];
        $comment .= '<br>Delivery Price: '.$order['delivery_price'];
        $comment .= '<br>Тип оплаты: '.$order['payment_type']['title'];
        $comment .= '<br>Стоимость заказа: '.$order['total_sum'].' '.$order['currency'];
        $comment .= '<br>Дата создания заказа: '.$order['stat_created'];
        $comment .= '<br>Комментарий: '.$order['comment'];
        $products = '';
        for($i=0;$i<count($order['products']);$i++)
        {
            $products .= $order['products'][$i]['quantity'].'xШТ. '.$order['products'][$i]['title'];
            if($i!=(count($order['products'])-1))
                $products .= ' + ';
        }
        $total_sum = 0;
        switch ($order['payment_type']['title'])
        {
            case 'Безналичный расчет без НДС (доплата +10%)':
                $total_sum += $order['total_sum']+($order['total_sum']*0.1);
                break;
            case 'Безналичный расчет с НДС (доплата +30%)':
                $total_sum += $order['total_sum']+($order['total_sum']*0.1);
                break;
        }

        $order_to_create_deal = array(
            'TITLE'=>$products,
            'OPPORTUNITY'=>$total_sum,
            'CURRENCY_ID'=>$order['currency'],
            'COMMENTS'=>$json_order,
            'CONTACT_ID'=>$id_contact,
            'ASSIGNED_BY_ID'=>1,
            'TYPE_ID'=>'148',
            'SOURCE_ID'=>'STORE',
            'UF_CRM_1521234711536'=>'99',
            'UF_CRM_1521235061375'=>$order['delivery_city'].', '.$order['delivery_address'],
            'UF_CRM_1521235035477'=>$order['order_id'],
            'UF_CRM_1537200904589'=>$order['payment_type']['title'],
            'UF_CRM_1537200932073'=>$order['delivery_type']['title'],
            'UF_CRM_1521234485781'=>$total_sum,
            'UF_CRM_1530812793360'=>'0',
            'UF_CRM_1530812815887'=>'0',
            'UF_CRM_1538435705'=>$products,
            'BEGINDATE'=>$order['stat_created'],
            'OPENED'=>'Y'
        );
        $result_create_deal = $this->createDeal($order_to_create_deal);
    }
    public function hookServices()
    {

        $export = new \horoshop\order\Export(isset(Z::$app->hs->checkout->use_countries) && Z::$app->hs->checkout->use_countries, false);

        $orders = $export->id($this->cart->order_id)->process();

        if ($orders) {
            $order = array_shift($orders);
            \horoshop\hooks\Service::getInstance()->hook('order_created', $order);
            $this->main_b24_integration($order);
        }
    }

    public function getId()
    {
        return $this->cart->id;
    }
}
